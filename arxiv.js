let remote_date = new Date();
const delay = 30 * 30 * 24 * 3600 * 1000;
const categories = ['astro-ph', 'astro-ph.CO', 'astro-ph.EP', 'astro-ph.GA', 'astro-ph.HE', 'astro-ph.IM', 'astro-ph.SR', 'cond-mat.dis-nn', 'cond-mat.mes-hall', 'cond-mat.mtrl-sci',
	'cond-mat.other', 'cond-mat.quant-gas', 'cond-mat.soft', 'cond-mat.stat-mech', 'cond-mat.str-el', 'cond-mat.supr-con', 'cs.AI', 'cs.AR', 'cs.CC', 'cs.CE',
	'cs.CG', 'cs.CL', 'cs.CR', 'cs.CV', 'cs.CY', 'cs.DB', 'cs.DC', 'cs.DL', 'cs.DM', 'cs.DS',
	'cs.ET', 'cs.FL', 'cs.GL', 'cs.GR', 'cs.GT', 'cs.HC', 'cs.IR', 'cs.IT', 'cs.LG', 'cs.LO',
	'cs.MA', 'cs.MM', 'cs.MS', 'cs.NA', 'cs.NE', 'cs.NI', 'cs.OH', 'cs.OS', 'cs.PF', 'cs.PL',
	'cs.RO', 'cs.SC', 'cs.SD', 'cs.SE', 'cs.SI', 'cs.SY', 'econ.EM', 'eess.AS', 'eess.IV', 'eess.SP',
	'gr-qc', 'hep-ex', 'hep-lat', 'hep-ph', 'hep-th', 'math.AC', 'math.AG', 'math.AP', 'math.AT', 'math.CA',
	'math.CO', 'math.CT', 'math.CV', 'math.DG', 'math.DS', 'math.FA', 'math.GM', 'math.GN', 'math.GR', 'math.GT',
	'math.HO', 'math.IT', 'math.KT', 'math.LO', 'math.MG', 'math.MP', 'math.NA', 'math.NT', 'math.OA', 'math.OC',
	'math.PR', 'math.QA', 'math.RA', 'math.RT', 'math.SG', 'math.SP', 'math.ST', 'math-ph', 'nlin.AO', 'nlin.CD',
	'nlin.CG', 'nlin.PS', 'nlin.SI', 'nucl-ex', 'nucl-th', 'physics.acc-ph', 'physics.ao-ph', 'physics.app-ph', 'physics.atm-clus', 'physics.atom-ph',
	'physics.bio-ph', 'physics.chem-ph', 'physics.class-ph,', 'physics.comp-ph', 'physics.data-an', 'physics.ed-ph', 'physics.flu-dyn', 'physics.gen-ph', 'physics.geo-ph', 'physics.hist-ph',
	'physics.ins-det', 'physics.med-ph', 'physics.optics', 'physics.plasm-ph', 'physics.pop-ph', 'physics.soc-ph', 'physics.space-ph', 'q-bio.BM', 'q-bio.CB', 'q-bio.GN',
	'q-bio.MN', 'q-bio.NC', 'q-bio.OT', 'q-bio.PE', 'q-bio.QM', 'q-bio.SC', 'q-bio.TO', 'q-fin.CP', 'q-fin.EC', 'q-fin.GN',
	'q-fin.MF', 'q-fin.PM', 'q-fin.PR', 'q-fin.RM', 'q-fin.ST', 'q-fin.TR', 'quant-ph', 'stat.AP', 'stat.CO', 'stat.ME',
	'stat.ML', 'stat.OT', 'stat.TH'];
let ncat = 0;
for(let cat of categories) {
	let number = 200;
	jQuery.ajax('http://export.arxiv.org/api/query?search_query=cat:'+cat+'&start=0&max_results=0', {
		crossDomain: true,
		async: false,
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus);
		},
		success: function(data, textStatus, jqXHR) {
			let feed = data.firstElementChild;
			number = feed.children[4].innerHTML;
			console.log(cat+": "+number);
		}
	});
	for(let i = 0; i < number; i += 500) {
		jQuery.ajax('http://export.arxiv.org/api/query?search_query=cat:'+cat+'&start='+i+'&max_results=500', {
			crossDomain: true,
			async: false,
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus);
			},
			success: function(data, textStatus, jqXHR) {
				let feed = data.firstElementChild;
				for(let i = 7; i < feed.children.length; i++) {
					let entry = feed.children[i];
					let id = entry.children[0].innerHTML;
					let publication = entry.children[2].innerHTML;
					let comments = entry.children[10];
					let pages;
					let error = false;
					if(comments != undefined) {
						comments = comments.innerHTML;
						let re = /([0-9]+) page/g;
						let res = re.exec(comments);
						if(res != null && re.exec(comments) == null) {
							pages = res[1];
						}
					}
					$('#results').append("<br/>\n"+id+";"+publication+";"+pages+";"+cat);
				}
				console.log(cat+": "+i+"/"+number+" ("+ncat+"/"+categories.length+")");
			}
		});
	}
	ncat++;
}
